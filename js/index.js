var xml;
var xmlToArray;
var positionCheckedTags;
var leafPosCheckedTags;
var params;
var link; 

var childs; //tags folha selecionadas na primeira pagina 
var tagsCounter;

var idMainTag = "";
var idAttrName = "";
var idAttrValue = "";
var idInfoTag = [];
var url_arcgisonline ="";
var xmlIdentificador;

//var labelHide_idConfiguration; // Para mostrar a label antes escondida

$(function () {
  $('#error-alert-getTags').hide();
  $("#success-alert").hide();
  $('#error-alert').hide();
  $('[data-toggle="tooltip"]').tooltip();
  //$( "#tags_url" ).trigger( "click" );  
});

$("#showxml_btn").click(function(e) {
    e.preventDefault();
});

// Voltar a pagina inicial
$("#prev_button").click(function(e) {
	e.preventDefault(); 
	$('#url_step2').hide();
  $('#url_step3').hide();
  $('#getTags_form').show();
	$('#url_response').show();
	$('#prev_button').hide();
	$('#config_tags').hide();
	$('#next_button').show();
  $('#next_button_end').hide();
  $('#prev_button_end').hide();

  uncheckTags();
});

// Carregar no botao getTags
$("#tags_url").click(function(e) {
  e.preventDefault();
  $('#url_response').show();
  $('#url_step2').hide();
  $('#url_step3').hide();
  $('#container_loading').show();
  $('#checkboxes_tags').hide();

  link = $('#exampleInputEmail1').val(); //buscar o url inserido

  if(link=="") {
    $('#container_loading').hide();
    return;
  }
  var params0 = link.split('?')[1];

  var params1 = params0.split('&');
  //console.log(params1);

  params = [];
  $.each(params1, function(i, val){
    params.push(val.split('=')[0]);
  });
  //console.log(params);
  //link = 'http://arquivodigital.eshte.pt/Nyron/Museum/Catalog/winlibsrch.aspx?skey=&pesq=2&doc=880&GetXML=true';
  //link = 'http://patrimoniocultural.cm-lisboa.pt/wsinpatrimonium/SFIMVWS.asmx/Imoveis_XML?NumImovel=LxConv110&Designacao=&ta_Tema=&ta_LocalAdministrativo=&ta_EntidadeInformacao=&sLang=&sTodasImagens=0&sImagensMaterialGrafico=0&sPassw=';

  //Fazer pedido ajax para invocacao por XML para ir buscar as tags
    $.ajax({ 
            url: link,
            type: "GET",
            crossDomain: true,
            dataType: "xml",
            success: function(data_xml){
              //console.log("info success: "+data_xml);
            	xml = data_xml;
            	$('#prev_button').hide();
              $('#prev_button_end').hide();
              $('#next_button_end').hide();
            	$('#config_tags').hide();
            	$('#checkboxes_tags').empty(); // Limpar o conteudo onde estarao as tags
            	$('#checkboxes_tags').show();
    			

            	$('#showxml_btn').removeAttr('disabled'); //Colocar o botao ShowXML disponivel 
            	
            	$('#next_button').show(); // Aparecer o botao Submit
            	//Colocar no popup o XML recebido
              //$('#textareaID').val(data_xml.documentElement);
              //console.log("bu " + new XMLSerializer().serializeToString(data_xml.documentElement));
              //console.log($(data_xml)[0].firstChild);
            	$('#textareaID').val(new XMLSerializer().serializeToString($(data_xml)[0].firstChild));
            	//$('#textareaID').val($(data_xml)[0].firstChild);
            	var tagNames = []; // Guarda as tags sem duplicados (Para verificar se ja existem)
            	var xmlElems = []; // Guarda os elementos inseridos em cada tag
            	var tagNames_counter = 0;


              xmlToArray = getXMLToArray(xml);  
              positionCheckedTags = [];

              //console.log(xmlToArray.length);

            	$('#container_loading').hide();

              var panelgroup = $('<div class="panel-group" style="height: 83%;" id="accordion0"></div>');

              var panel = $('<div class="panel panel-default"></div>');
              
              //var btngroup = $('<div class="row"></div>');
              //$('#checkboxes_tags').append(btngroup);
              $('#checkboxes_tags').append("<h5>Selecione as tags que contêm a informação adicional pretendida bem como o identificador de ligação com o ArcGIS Server: </h5>"); 
              $('#checkboxes_tags').append(panelgroup);
              panelgroup.append(panel);

              //var childList = $('<ul class="list-group checked-list-box"></ul>');
              //btngroup.append(childList);

              // Criacao logo tambem do collapse da ligacao (page3)              
              /*var container3 = $('#url_step3');

              var panelgroup3 = $('<div class="panel-group" id="connection0"></div>');

              var panel3 = $('<div class="panel panel-default"></div>');
              
              //$('#connectionDiv0').append("<h5>Defina as tags que contêm a informação a mostrar: </h5>"); 

              var collapseDiv0 = $('<div id="connectionCollapse"></div>'); 
              $('#connectionDiv0').append(collapseDiv0);
              collapseDiv0.append(panelgroup3);
              panelgroup3.append(panel3);
              */

              //createListCollapse(data_xml, panel, 0);

              //createCollapseByArray(xmlToArray, panel, panel3, 0);
              createCollapseByArray(xmlToArray, panel, 0);

              $('#accordion0').enscroll({
                  showOnHover: false,
                  verticalTrackClass: 'track3',
                  verticalHandleClass: 'handle3'
              });

              /*

                  $(data_xml).children().each( function(index, element){            

                  
                    	//addCheckbox(this.tagName); // Insercao da checkbox antes da tag 
                    	xmlElems[tagNames_counter] = this;
                    	tagNames[tagNames_counter++] = this.tagName;

                      //$('<li class="list-group-item" data-style="button" data-color="success"> &lt;' 
                      //  + this.tagName + '&gt; </li>').appendTo(childList);
                      var collapse1 = $('<div class="panel-heading"></div>');
                      var title_collapse = $('<h4 class="panel-title"></h4>');

                      panel.append(collapse1);
                      collapse1.append(title_collapse);

                      $('<a data-toggle="collapse" data-parent="#accordion'+tagNames_counter+'" href="#collapse'+tagNames_counter+'"> '
                            + this.tagName + ' </a>').appendTo(title_collapse);   

                      var collapse_child = $('<div id="collapse'+tagNames_counter+'" class="panel-collapse collapse"></div>');
                      panel.append(collapse_child);

                      var collapse_body = $('<div class="panel-body"></div>');
                      collapse_child.append(collapse_body);

                      //Guardar os filhos para nao repetir
                      var childrenElems = [];
                      var childrenCount = 0;

                      var panelgroup_child = $('<div class="panel-group" id="accordion'+tagNames_counter+'_0"></div>');
                      var panel_child = $('<div class="panel panel-default"></div>');
                      collapse_body.append(panelgroup_child);
                      panelgroup_child.append(panel_child);

                      $(xmlElems[tagNames_counter-1]).children().each(function(index, element) {  
                        if(jQuery.inArray(this.tagName, childrenElems) == -1){ // Caso ainda nao exista essa tag no array (nao haver duplicados)
                          childrenElems[childrenCount++] = this.tagName;

                          var collapse1_1 = $('<div class="panel-heading"></div>');
                          var title_collapse_1 = $('<h4 class="panel-title"></h4>');

                          panel_child.append(collapse1_1);
                          collapse1_1.append(title_collapse_1);

                          $('<a data-toggle="collapse" data-parent="#accordion'+tagNames_counter+'_0" href="#collapseInner'
                            +tagNames_counter+'_'+childrenCount+'"> '+ this.tagName + ' </a>').appendTo(title_collapse_1);   

                          var collapse_child_1 = $('<div id="collapseInner'+tagNames_counter+'_'+childrenCount+'" class="panel-collapse collapse"></div>');
                          panel_child.append(collapse_child_1);
                          var collapse_body = $('<div class="panel-body" >'+ this.tagName+' </div>');
                          collapse_child_1.append(collapse_body);

                        } 
                                                
              });

                /*
                  <div class="row">
                        <h3 class="text-center">Using Button Style's</h3>
                        <div class="well" style="max-height: 300px;overflow: auto;">
                        <ul class="list-group checked-list-box">
                              <li class="list-group-item" data-style="button">Cras justo odio</li>
                              <li class="list-group-item" data-style="button" data-color="success">Dapibus ac facilisis in</li>
                              <li class="list-group-item" data-style="button" data-color="info">Morbi leo risus</li>
                              <li class="list-group-item" data-style="button" data-color="warning">Porta ac consectetur ac</li>
                              <li class="list-group-item" data-style="button" data-color="danger">Vestibulum at eros</li>
                            </ul>
                        </div>
                  </div>
                */


                /******************* COM BOTOES *************************/

                /*
                  var btngroup = $('<div class="btn-group"></div>');
                  $('#checkboxes_tags').append(btngroup);

                  var spancheckbox = $('<span class="button-checkbox checkboxChangeBtn"></span>');
                  var buttonfirst = $('<button type="button" class="btn btn-default checkboxRadiusRight"> &lt' + this.tagName + '&gt; </button>'
                                      + '<input type="checkbox" class="hidden" />');
                  var buttondrop = $('<button type="button" class="btn btn-default checkboxRadiusLeft" data-toggle="collapse"'
                                      + 'data-target="#collapseExample" aria-controls="collapseExample'
                                      + ' aria-expanded="false">'
                                      + '<span class="caret"></span>'
                                      + '<span class="sr-only">Toggle Dropdown</span>'
                                      + '</button>');


                  btngroup.append(spancheckbox);
                  spancheckbox.append(buttonfirst);
                  checkboxChange();
                  spancheckbox.append(buttondrop);

                  var collapse_chbox = $('<div class="collapse" id="collapseExample"></div>');
                  var div_collapse = $('<div class="card card-block"></div>');
                  spancheckbox.append(collapse_chbox);
                  collapse_chbox.append(div_collapse);

                  //Guardar os filhos para nao repetir
                  var childrenElems = [];
                  var childrenCount = 0;

                  var btngroup = $('<div class="btn-group"></div>');
                  div_collapse.append(btngroup);

                  $(xmlElems[tagNames_counter-1]).children().each(function(index, element) {  
                    if(jQuery.inArray(this.tagName, childrenElems) == -1){ // Caso ainda nao exista essa tag no array (nao haver duplicados)
                      childrenElems[childrenCount++] = this.tagName;

                      var spancheckbox = $('<span class="button-checkbox checkboxChangeBtn"></span>');
                      var buttonfirst = $('<button type="button" class="btn btn-default checkboxRadiusRight"> &lt' + this.tagName + '&gt; </button>'
                                      + '<input type="checkbox" class="hidden" />');
                      var buttondrop = $('<button type="button" class="btn btn-default checkboxRadiusLeft" data-toggle="collapse"'
                                      + 'data-target="#collapseExample" aria-controls="collapseExample'
                                      + ' aria-expanded="false">'
                                      + '<span class="caret"></span>'
                                      + '<span class="sr-only">Toggle Dropdown</span>'
                                      + '</button>');
                      btngroup.append(spancheckbox);
                      spancheckbox.append(buttonfirst);
                      checkboxChange();                
                      spancheckbox.append(buttondrop);
                    }                                
                  });
                */
              //});
              //listCheckBox();
              /*
                for(var i = 0; i < tagNames_counter; i++){
                	$(xmlElems[i]).children().each(function(index, element) {  

                		if(jQuery.inArray(this.tagName, tagNames) == -1){ // Caso ainda nao exista essa tag no array (nao haver duplicados)
                  		addCheckbox(this.tagName);
  	                  	xmlElems[tagNames_counter] = this;
  	                  	tagNames[tagNames_counter++] = this.tagName;	
                		}                                
                	});
                }
              */
            },
            error: function(xhr, ajaxOptions, thrownError){
              //console.log("error: "+xhr);
              //console.log("status: "+xhr.status);
              //console.log("options: "+ajaxOptions);
              //console.log("error: "+thrownError);
            	$('#container_loading').hide();
            	$("#error-alert-getTags").fadeIn('slow');
              $("#error-alert-getTags").fadeTo(2000, 500).slideUp(500, function(){
                $("#error-alert-getTags").slideUp(500);
              });   
            }
    });

});

function find_multiArray(value, array, count){
  for(var i = 0; i < count; i++){
    if(array[i]['tag'] == value)
      return i;
  }
  return -1;
}

function getXMLToArray(xmlDoc){
    var thisArray = new Array();
    var elem = 0;
    //Check XML doc
    if($(xmlDoc).children().length > 0){
    //Foreach Node found
      $(xmlDoc).children().each(function(){ 
          //childArray = new Array();

          //thisArray[elem] = childArray;
          if(find_multiArray( this.nodeName, thisArray, elem)==-1){
            childArray = new Array();
            thisArray[elem] = childArray;
            childArray['tag'] = this.nodeName;
            childArray['check'] = false;
            childArray['attrs'] = this.attributes;

            if($(xmlDoc).find(this.nodeName).children().length > 0){
            //If it has children recursively get the inner array
              var NextNode = $(xmlDoc).find(this.nodeName);
              childArray['values'] =  getXMLToArray(NextNode);
              //thisArray[elem] = getXMLToArray(NextNode);
            } else {
            //If not then store the next value to the current array
              childArray['values'] = null;
            }
            elem++;
          }
      });
    }
   // console.log(thisArray);
    return thisArray;
}

// Ir para a pagina 2
$("#next_button").click(function(e) {
  e.preventDefault();
  $('#prev_button').show();
  $('#prev_button_end').hide();
  $('#next_button').hide();
  $('#next_button_end').show();
  //$('#next_button_end').hide();
  //$('#config_tags').show();
  $('#config_tags').hide();
  $('#url_response').hide();
  $('#url_step3').hide();
  $('#url_step2').show();
  $('#url_step2').empty();

  $('#getTags_form').hide();

  var container = $('#url_step2');
  container.append("<h5 style='margin-bottom: 2%;'>Personalize a informação a visualizar: </h5>"); 

  var form = $('<form id="config_form" class="btn-group" data-toggle="buttons"></form>');
  container.append(form);


  tagsChecked();

  childs = childsChecked(); // Recebe os nos das folhas selecionadas
  //console.log(childs);
  tagsCounter = [];
  
  for(var i = 0; i < childs.length; i++){

    tagsCounter[i] = 0;

    createTagConfig('childDiv_'+i, form, false);
  }

  $('#config_form').enscroll({
      showOnHover: false,
      verticalTrackClass: 'track3',
      verticalHandleClass: 'handle3'
  });
});

$("#prev_button_end").click(function(e) {
  e.preventDefault();
  $('#prev_button').show();
  $('#prev_button_end').hide();
  $('#next_button').hide();
  $('#next_button_end').show();
  $('#config_tags').hide();
  $('#url_response').hide();
  $('#url_step2').show();
  $('#url_step3').hide();
});

$("#arcGIS_submit").click(function(e) {
  e.preventDefault();
  $('#arcgisResponse').empty(); // Limpar o conteudo onde estarao as tags 
  $('#arcgisResponse').hide(); 
  $('#loading_arcgis').show();
  var link = $('#InputOtherUrl').val(); //buscar o url inserido
  arcgisonline(link);
});

function arcgisonline(link){
  var resultLink = link;
  //http://www.arcgis.com/home/webmap/viewer.html?webmap=3ad6ea12146d43c7aa3ec2ae56fb44ff
  var arcgis_split = link.split('=');
  if(arcgis_split.length > 1){
    resultLink = arcgis_split[1];
  }
  url_arcgisonline = resultLink;
  //console.log(resultLink);

  require([//"dojo/parser",
    //"dojo/ready",
    "dijit/layout/BorderContainer",
    "dijit/layout/ContentPane",
    //"dojo/dom",
    "esri/map",
    "esri/urlUtils",
    "esri/arcgis/utils",
    "esri/dijit/Legend",
    "esri/layers/FeatureLayer",
    "esri/dijit/Scalebar"
    //"dojo/domReady!"
    ], function(
    //parser,
    //ready,
    BorderContainer,
    ContentPane,
    //dom,
    Map,
    urlUtils,
    arcgisUtils,
    Legend,
    FeatureLayer,
    Scalebar) {
      //ready(function(){
        //console.log("entrou no require");
        arcgisUtils.createMap(resultLink,"map").then(function(response){
          //update the app
          //console.log(response.itemInfo);
          //console.log(response.itemInfo.itemData);
          //console.log(response.itemInfo.itemData.operationalLayers);
          //console.log(response.itemInfo.itemData.operationalLayers[0].featureCollection);
          //console.log(response.itemInfo.itemData.operationalLayers[0].featureCollection.layers);
          var layers = response.itemInfo.itemData.operationalLayers;
          console.log(layers);
          // PAra o caso de ser um csv ou um web service
          var layerInfos = [];
          var fieldsToJson = [];
          //console.log(layers);
          var eLayer;
          var span_dropdown_arcgis = $('<span id="dropdown_arcgis" class="custom-dropdown"></span>');
          var select_arcgis = $('<select></select>');
          select_arcgis.appendTo(span_dropdown_arcgis);
          span_dropdown_arcgis.appendTo($('#arcgisResponse'));
          $.each(layers, function(k,layer) {
            if(layer.visibility){
              select_arcgis.append('<option disabled>' + layer.title + '</option>');
              if(layer && layer.layerObject) {
                eLayer = layer.layerObject;
                var fields_layer = eLayer.infoTemplate.info.fieldInfos; 
                $.each(fields_layer, function(i, value){
                  if(value.visible)
                    select_arcgis.append('<option>' + value.fieldName + '</option>');
                });
                layerInfos.push({
                  "featureLayer": eLayer
                });
              }else {
                console.log('csv');
                var layersCsv = layer.featureCollection.layers;
                for(var i = 0; i < layersCsv.length; i++){
                  var fieldsCsV = layersCsv[i].layerDefinition.fields;
                  // Retirar o __OBJECTID dos fields, campo criado pelo proprio arcgis online
                  var idArcgis = "__OBJECTID";
                  fieldsCsV = jQuery.grep(fieldsCsV, function(value) {
                    return value.name != idArcgis;
                  });
                  $.each(fieldsCsV, function(i, value){
                    select_arcgis.append('<option>' + value.name + '</option>');
                  });
                }
              }
              //console.log(fieldsToJson);
            }
          });

          //$.each(layerInfos, function(m, layerinfo){}
          /*if(eLayer != null){ // é um servico web
            fieldsToJson = eLayer.fields;
          }else{ // Formato csv que nao tem layer
            var layersCsv = layers[0].featureCollection.layers;
            for(var i = 0; i < layersCsv.length; i++){
              var fieldsCsV = layersCsv[i].layerDefinition.fields;

              // Retirar o __OBJECTID dos fields, campo criado pelo proprio arcgis online
              var idArcgis = "__OBJECTID";
              fieldsCsV = jQuery.grep(fieldsCsV, function(value) {
                return value.name != idArcgis;
              });
              $.merge(fieldsToJson, fieldsCsV);
            }
          }*/

          $('#loading_arcgis').hide();      
          $('#arcgisResponse').show();
          //var span_dropdown_arcgis = $('<span id="dropdown_arcgis" class="custom-dropdown"></span>');
          //var select_arcgis = $('<select></select>');
          //select_arcgis.appendTo(span_dropdown_arcgis);
          //span_dropdown_arcgis.appendTo($('#arcgisResponse'));
        });
      //});
    });
}
// Ir para a pagina 3
$("#next_button_end").click(function(e) {
  e.preventDefault();

  var json_elems = validateForm();
  //console.log(json_elems);

  if(json_elems != null){

    $('#prev_button').hide();
    $('#prev_button_end').show();
    $('#next_button').hide();
    $('#next_button_end').hide();
    $('#config_tags').show();
    $('#url_response').hide();
    $('#url_step2').hide();
    $('#url_step3').show();
    //$('#url_step3').empty();
    var container = $('#url_step3');


    $('#resumo_id').empty();
   
    $('#params_id').empty();

    $('#params_id').append('<h5><strong>Identificador nos paramêtros do URL:</strong></h5>');

    var span_dropdown = $('<span id="dropdown_params" class="custom-dropdown"></span>');
    var select = $('<select></select>');
    select.appendTo(span_dropdown);
    span_dropdown.appendTo($('#params_id'));
    $.each(params, function(i, value){
      select.append('<option>' + value + '</option>');
    });

    $('#params_id').append('<p>URL inserido: ' + link + '</p>');
    
  }
});


// Buscar as tags selecionadas e colocar check = true no array 
// Importante para o caso de voltar atras e querer desselecionar 
var tagsChecked = function(){
  var tagsCheck = [];

  $('#checkboxes_tags .glyphicon-check').each(function(index, element) { 
    var id_elem = element.id;
    var split_array = id_elem.split("_");
    //console.log(split_array);
    //console.log(split_array.length);

    //os primeiros dois elementos nao se referem as posicoes no array 'xmlToArray'
    var tagP = xmlToArray[split_array[2]]; 
    var stringPosition = split_array[2];
    //console.log(tagP.values[0]);
    for(var i = 3; i < split_array.length; i++){
      var tmp = split_array[i]; // Guarda a posicao da tag filha nos values da tag pai 
      stringPosition = stringPosition + "_" + tmp;
      tagP = tagP.values[tmp]; 
    }
    // Saber quais é que estao check 
    tagP.check = true;
    positionCheckedTags.push(stringPosition);
    tagsCheck.push(tagP.tag);
    //console.log(tagP);
  });
}

// Metodo para criacao da dropdown com os valores do atributo selecionado
//function createDropdownAttrValues(tagName, attrName, idButton){
function createDropdownAttrValues(tagName, attrName, divToAppend){  
  //var tagAttribute = $(attrName).text(); // Nome do atributo
  var tagAttribute = attrName;
  var leafTag = tagName; // Nome da tag

  //console.log("AttrValues for: "+tagName+" > "+tagAttribute);

  var dropdownAttrValues = $('<span id="dropdownAttrValues_'+ tagName +'" class="custom-dropdown dropdown_attrValues"></span>');
  var selectAttrValues = $('<select></select>');
  //dropdownAttrValues.insertAfter(divToAppendDrop);
  dropdownAttrValues.appendTo(divToAppend);
  selectAttrValues.appendTo(dropdownAttrValues);

  var allNodesTag = $(xml).find(leafTag); // Buscar todos os elementos do xml com essa tag
  
  var allAttrNames = [];
  allNodesTag.each(function(index, element){ // Buscar todos os valores do atributo selecionado
    //console.log($(this).attr(tagAttribute));
    var name = $(this).attr(tagAttribute);
    if(jQuery.inArray(name, allAttrNames) == -1){ // Para nao existir nomes repetidos
      allAttrNames.push(name);
    }
  });
  allAttrNames.sort(); // Ordenar os valores da dropdown
  $.each(allAttrNames, function(index, element){
    selectAttrValues.append('<option>' + element + '</option>');
    //   $('<li><a href="#">'+ element +'</a></li>').appendTo(listAttrValues);
  });
  return dropdownAttrValues;
}

// Colocar todas as tags check como uncheck 
function uncheckTags(){
  //console.log(positionCheckedTags);
  for(var i = 0; i < positionCheckedTags.length; i++){
    //console.log( positionCheckedTags[i]);
    var pos_array = positionCheckedTags[i].split("_"); // guarda as posicoes da tag dentro do xmlToArray
    //console.log(pos_array);
    var elemUnchecked = xmlToArray[pos_array[0]];
    elemUnchecked.check = false;
    for(var j = 1; j < pos_array.length; j++){
      elemUnchecked = elemUnchecked.values[pos_array[j]];
      elemUnchecked.check = false;
    }
  }
  positionCheckedTags = [];
}

//Colocar so as tags filhas selecionadas na pagina 
var childsChecked = function(){
  var root = [];
 // leafPosCheckedTags = [];
  //buscar os pais (raiz) 
  for( var i = 0; i < xmlToArray.length; i++){
    if(xmlToArray[i].check){
      root.push(xmlToArray[i]);
   //   leafPosCheckedTags.push(i);
    }
  }

  var result = [];
  //Entrar nos filhos 
  while(root.length > 0){
    var elem = root.shift(); // retira o primeiro elemento do array
    //var pos = leafPosCheckedTags.shift();
    //console.log(elem);
    var hasChildChecked = false;
    for(var j = 0 ; elem.values != null && j < elem.values.length; j++){
      if(elem.values[j].check){
        hasChildChecked = true;
        root.push(elem.values[j]); // coloca no fim do array
        //leafPosCheckedTags.push(pos+"_"+j);
      }
    }
    if(!hasChildChecked){ // Caso nao tenha filhos selecionados, ele proprio e o ultima tag checked
      result.push(elem);
      //leafPosCheckedTags.push(pos);
    }
    //console.log(childs);
  }
  //console.log(leafPosCheckedTags);
  return result;
}

var capitalizeLabel = function(string) {    
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase;
}

// User faz submit
$('#config_tags').click(function(e) {
	e.preventDefault();

  var json_elems = validateForm();

   // {id_arcgis: _____, id_webmap: _____, info: [_______]}
  var infoToJson = new Object();

  infoToJson.url_arcgis = url_arcgisonline;
  infoToJson.id_arcgis = $('#dropdown_arcgis select')[0].value;


  infoToJson.id_info_param = $('#dropdown_params select')[0].value;
  infoToJson.url_info = link;

  //console.log("ID: "+infoToJson.id_info_tag);
  //infoToJson.id_arcgis = $('input[name=radioFieldArcgis]:checked').val();
  //console.log("ID: "+infoToJson.id_arcgis);

  infoToJson.info = json_elems;

  //console.log("json:");
  //console.log(JSON.stringify(infoToJson));
  //console.log(infoToJson);
  if(json_elems != null){
    $.ajax
      ({
          type: "POST",
          dataType : 'json',
          //contentType: 'text/plain',
          async: true,
          url: 'php/generate_txt.php',
          data: { data: JSON.stringify(infoToJson) },
          //success: function () {alert("Success!"); },
          //failure: function() {alert("Error!");}
      }).done(function(data) {
          //alert('done!');
           //$("#success-alert").show();
           $("#success-alert").fadeIn('slow');
            $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
              $("#success-alert").slideUp(500);
            });   
      }).fail(function(request,status, error) {
        //console.log("vem daqui?");
        $("#error-alert").fadeIn('slow');
        $("#error-alert").fadeTo(2000, 500).slideUp(500, function(){
          $("#error-alert").slideUp(500);
        }); 
        //console.log(request);
        //console.log(error);
        //console.log('Error: '+status);
      });	
  }
});

var validateForm = function(){

  var validate = [];
  var finalValidation = true;

  $('#url_step2 > form .tagConfig').each(function(index, element) {  
    //tags_file.push(this.value);
    var validateElem = true;

    var tag_info = new Object();
    //console.log(element);
    
    //console.log($(this));
    var tag_label = $($(this).parents('.newTagElem').find('.panel-heading .panel-title')[0]).text(); //Obtem a tag em uso - a label que aparece logo a esquerda

   
    tag_info.tag_name = tag_label.trim();
    
    tag_info.attrName = "";
    tag_info.attrValue = "";
    //console.log("> " + tag_label); //Esta certo, nome da tag

    var child_id = element.id;

    /*************** Validacao do atributo*****************/
    
    var values = $($(this).find('.toggle')[0]); // Buscar todos os botoes relacionados com a configuracao de cada tag 
   
    
    if(!values.find('input').is(':disabled') && !values.hasClass('off')){
      var attrButtons = $(this).find('.dropdown_attrNames select');
      var attrName = attrButtons[0].value;
      tag_info.attrName = attrName;

      attrButtons = $(this).find('.dropdown_attrValues select');
      var attrValue = attrButtons[0].value;
      tag_info.attrValue = attrValue;
    }
    
    
    /***************** Validacao da Label *****************/
   
      var labelOptions = $(this).find('#radio-'+ child_id + ' > div');

      //Caso haja 2 ou 3 opçoes de configuracao da label
      if(labelOptions.length == 3){
            
        var labelCheck = labelOptions.find('.glyphicon-check');
        if(labelCheck.length != 0 ){
          var valueLabel = labelCheck.parent().siblings('.collapse').find('.buttonConfig');

          if($(valueLabel[0]).hasClass('dropdown_tagLabel')){
            tag_info.label = $(valueLabel[0]).find('select')[0].value;
            //console.log(tag_info.label);
          } else if($(valueLabel[0]).hasClass('dropdown_tagInfoLabel')){
            tag_info.label = $(valueLabel[0]).find('select')[0].value;
            tag_info.value_inLabel = 'true';
            //console.log(tag_info.label);
          } else{
            //console.log(valueLabel.prop('value'));
            tag_info.label = valueLabel.prop('value').trim();
          }
          //console.log(valueLabel);
          if(! verifyValidation(valueLabel, tag_info.label)){
            validateElem = false;
          }else {
            labelOptions.parent().parent().removeClass('error-validation');
            labelOptions.parent().parent().siblings('.errorp').remove();
          }
        }else { // QUando nao ha nenhuma checkbox ativada
          verifyValidation(labelOptions.parent().parent(), "");
          validateElem = false;
        }
      }else if(labelOptions.length == 2){ 
        //console.log('option = 2');
        tag_info.value_inLabel = "false";

        var valueLabel = labelOptions.find('.glyphicon-check');

        var valueLabelId = valueLabel.attr('id');

        //var parentId = labelOptions.parent().parent().attr('id');
        //var optionLabel = parentId.split('-');

        var optionLabel = valueLabelId.split('-'); //Verificar qual das opçoes
        //console.log('qual check: '+optionLabel[0]);
        if(optionLabel[0] == 'check1'){
          //console.log('Label : '+valueLabel.parent().text()); // esta certo 
          tag_info.label = valueLabel.parent().text();
          if(! verifyValidation(valueLabel.parent(), tag_info.label)){
            validateElem = false;
          }
        }else if(optionLabel[0] == 'check2'){
          tag_info.label = valueLabel.parent().siblings('input').prop("value").trim();
          //console.log('Label check2: '+tag_info.label);
          if(! verifyValidation(valueLabel.parent().siblings('input'), tag_info.label)){
            validateElem = false;
          }
        }
      }
    //}

   /******************* Parte da tag da informacao *********************/
   //var infoLabel = $(this).find('.textValuesDiv .buttonConfig');
   //console.log( $(this));
   var infoLabel = $($(this).find('.textValuesDiv')[0]).find('.infoTagDivs');
   //console.log(infoLabel.length);
   //console.log(infoLabel);
   //console.log(infoLabel[0].value);

   tag_info.value = [];
   var allSelectsInfo;

  $.each(infoLabel, function(j, elem){
    allSelectsInfo = new Object();
    var valueSelect = $(elem).find('.dropdown_tagInfo select')[0].value;
    allSelectsInfo.tag = valueSelect;
    var toggle = $(elem).find('.toggle input');
    if(toggle.prop('checked')){
      allSelectsInfo.attrName = $(elem).find('.dropdown_attrNames2 select')[0].value;
      allSelectsInfo.attrValue = $(elem).find('.dropdown_attrValues select')[0].value;
    }else{
      allSelectsInfo.attrName = "";
      allSelectsInfo.attrValue = "";
    }
    tag_info.value.push(allSelectsInfo);
  });

   
    /**************** Verificar se é identificador ********************/
    if(!validateElem){
      finalValidation = false;
    }else{
      if(finalValidation){
        validate.push(tag_info);
      }
    }
  });
  
  if(finalValidation){
    return validate;
  }
  return null;
}

var verifyValidation = function(element, value){
  //console.log(element);
  var validation = true;
  if(value == ""){
    validation = false;
    element.removeClass('ok-validation');
    element.addClass('error-validation');
    element.siblings('.errorp').remove();
    $('<p class="errorp" style="color: red;"> <strong> Campo obrigatório! </strong> </p>').insertAfter(element);
  }else{
    element.removeClass('error-validation');
    element.addClass('ok-validation');
    element.siblings('.errorp').remove();
  }
  return validation;
}

var checkboxChange = function(){
  $('.checkboxChangeBtn').each(function () {

    // Settings
    var $widget = $(this),
        $button = $widget.find('button'),
        $checkbox = $widget.find('input:checkbox'),
        color = $button.data('color'),
        settings = {
            on: {
                icon: 'glyphicon glyphicon-check'
            },
            off: {
                icon: 'glyphicon glyphicon-unchecked'
            }
        }
      ;
      // Event Handlers
      $button.on('click', function () {
          $checkbox.prop('checked', !$checkbox.is(':checked'));
          $checkbox.triggerHandler('change');
          updateDisplay();
      });
      $checkbox.on('change', function () {
          updateDisplay();
      });

    // Actions
    function updateDisplay() {
        var isChecked = $checkbox.is(':checked');

        // Set the button's state
        $button.data('state', (isChecked) ? "on" : "off");

        // Set the button's icon
        $button.find('.state-icon')
            .removeClass()
            .addClass('state-icon ' + settings[$button.data('state')].icon);

        // Update the button's color
        if (isChecked) {
            $button
                .removeClass('btn-default')
                .addClass('btn-' + color + ' active');
        }
        else {
            $button
                .removeClass('btn-' + color + ' active')
                .addClass('btn-default');
        }
    }

    // Initialization
    function init() {

        updateDisplay();

        // Inject the icon if applicable
        if ($button.find('.state-icon').length == 0) {
            $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
        }
    }
    init();
  });
}

var listCheckBox = function(){
   $('.list-group.checked-list-box .list-group-item').each(function () {
        
        // Settings
        var $widget = $(this),
            $checkbox = $('<input type="checkbox" class="hidden" />'),
            color = ($widget.data('color') ? $widget.data('color') : "primary"),
            style = ($widget.data('style') == "button" ? "btn-" : "list-group-item-"),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };
            
        $widget.css('cursor', 'pointer')
        $widget.append($checkbox);

        // Event Handlers
        $widget.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });
          

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $widget.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $widget.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$widget.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $widget.addClass(style + color + ' active');
            } else {
                $widget.removeClass(style + color + ' active');
            }
        }

        // Initialization
        function init() {
            
            if ($widget.data('checked') == true) {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
            }
            
            updateDisplay();

            // Inject the icon if applicable
            if ($widget.find('.state-icon').length == 0) {
                $widget.prepend('<span class="state-icon ' + settings[$widget.data('state')].icon + '"></span>');
            }
        }
        init();
    });
    
    $('#get-checked-data').on('click', function(event) {
        event.preventDefault(); 
        var checkedItems = {}, counter = 0;
        $("#check-list-box li.active").each(function(idx, li) {
            checkedItems[counter] = $(li).text();
            counter++;
        });
        $('#display-json').html(JSON.stringify(checkedItems, null, '\t'));
    });
}

// Utiliza o array que contem a informacao do xml recebido do pedido ajax
//var createCollapseByArray = function(array, panel, panelRadio, idParent){
var createCollapseByArray = function(array, panel, idParent){
  //Guardar os filhos para nao repetir
  var childrenElems = [];
  var childrenCount = 0;

  for(var idx = 0; idx < array.length; idx++){

    var collapse1 = $('<div class="panel-heading "></div>');
    var title_collapse = $('<h4 class="panel-title"></h4>');

    panel.append(collapse1);
    collapse1.append(title_collapse);

    if(array[idx].values!= null){

      // Heading - Contem o title
      var collapse_elem = $('<a data-toggle="collapse" data-parent="#accordion'+idParent
        +'" href="#collapse'+idParent+'_'+idx+'"> '
        + '<span id="open_'+idParent+'_'+idx+'" class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> '
        + '<span id="check_'+idParent+'_'+idx+'"class="glyphicon glyphicon-unchecked" aria-hidden="true"></span> '
        + ' &lt;'
        +  array[idx].tag + '&gt; </a>');
      collapse_elem.appendTo(title_collapse);  

      
      // Quando se clica numa checkbox
      collapse_elem.on('click', function(){

        var id_checkbox = $(this).find('span')[1].id;
        var id_arrow = $(this).find('span')[0].id;
        var checkbox_elem = $('#'+id_checkbox);
        var arrow_elem = $('#'+id_arrow);
        //alterar a checkbox
        var myClasses = checkbox_elem[0].classList;

        if(myClasses[1] == 'glyphicon-unchecked'){
          $(checkbox_elem).removeClass('glyphicon-unchecked');
          $(checkbox_elem).addClass('glyphicon-check');
          $(checkbox_elem).css("color", "#337AB7");

          $(arrow_elem).removeClass('glyphicon-menu-right');
          $(arrow_elem).addClass('glyphicon-menu-down');

        } else {
          $(checkbox_elem).addClass('glyphicon-unchecked');
          $(checkbox_elem).removeClass('glyphicon-check');
          $(checkbox_elem).css("color", "black");

          $(arrow_elem).removeClass('glyphicon-menu-down');    
          $(arrow_elem).addClass('glyphicon-menu-right');

          //Retirar o tick das checkboxes filhas
          id_divClose = $(this)[0].hash;
          $(id_divClose).find(".glyphicon-check").addClass('glyphicon-unchecked');
          $(id_divClose).find(".glyphicon-check").css("color", "black");
          $(id_divClose).find(".glyphicon-check").removeClass('glyphicon-check');

          //Retirar o collapse das filhas
          $(id_divClose).find(".collapse").removeClass('in');

          // Alterar a seta das filhas que tem mais filhas
          $(id_divClose).find(".glyphicon-menu-down").addClass('glyphicon-menu-right');
          $(id_divClose).find(".glyphicon-menu-down").removeClass('glyphicon-menu-down');

        }
      });

      
      // Body - Contem a informacao ( Os filhos )
      var collapse_child = $('<div id="collapse'+idParent+'_'+idx+'" class="panel-collapse collapse"></div>');
      panel.append(collapse_child);

      /*var collapse_child3 = $('<div id="collapse3'+idParent+'_'+idx+'" class="panel-collapse collapse"></div>');
      panelRadio.append(collapse_child3);*/

      var collapse_body = $('<div class="panel-body"></div>');
      collapse_child.append(collapse_body);

      /*var collapse_body3 = $('<div class="panel-body"></div>');
      collapse_child3.append(collapse_body3);*/


      var panelgroup_child = $('<div class="panel-group" id="accordion'+idParent+'_'+idx+'_00"></div>');
      var panel_child = $('<div class="panel panel-default"></div>');
      collapse_body.append(panelgroup_child);
      panelgroup_child.append(panel_child); 

      //createCollapseByArray(array[idx].values, panel_child, panel_child3, (idParent+'_'+idx));
      createCollapseByArray(array[idx].values, panel_child, (idParent+'_'+idx));
    
    }
    else {
      var checkspan = $('<span id="check_'+idParent+'_'+idx+'" class="glyphicon glyphicon-unchecked" aria-hidden="true"></span> &lt;'+array[idx].tag+'&gt; </inut></p>');
      checkspan.appendTo(title_collapse);

      // Quando se clica numa checkbox
      checkspan.on('click', function(){
          var myClasses = this.classList;
          if(myClasses[1] == 'glyphicon-unchecked'){
            $(this).removeClass('glyphicon-unchecked');
            $(this).addClass('glyphicon-check');
            $(this).css("color", "#337AB7");
          } else {
            $(this).addClass('glyphicon-unchecked');
            $(this).removeClass('glyphicon-check');
            $(this).css("color", "black");
          }
          //alert(myClasses.length + " " + myClasses[0]);
      });
    }
  }
}

// Criar div de configuracao para cada tag 
var createTagConfig = function(divName, parent, child){ 
  var parent_i;

  //console.log('parent: '+parent.id);

  var childDiv = $('<div id="'+divName+'" class="form-group" ></div>');

  var infoDiv;
  var buttonsDiv;

  if(child){ // Caso seja um campo ja existente, em que adicionar por baixo do que ja existia
    $('<hr>').appendTo(parent);
    childDiv.addClass('tagConfig');
    childDiv.appendTo(parent);
    parent_i = parent.id.split("_")[1];
    //console.log('child: '+parent_i);

    infoDiv = $('<div id="info_'+divName+'" class="form-group" ></div>');
    buttonsDiv = $('<div id="buttons_'+divName+'" class="form-group divButtons" ></div>');
    infoDiv.appendTo(childDiv);
    buttonsDiv.appendTo(childDiv);

    addMinusButton(buttonsDiv);
  }else{
    childDiv.addClass('newTagElem');
    childDiv.appendTo(parent);
    parent_i = divName.split("_")[1];
    //console.log('root: '+parent_i);
    childDiv = addCollapseTag(childDiv, divName, parent_i);
    //console.log('divName: '+divName);
    //console.log('suposto: '+ );
    divName = childDiv.attr('id');

    infoDiv = $('<div id="info_'+divName+'" class="form-group" ></div>');
    buttonsDiv = $('<div id="buttons_'+divName+'" class="form-group divButtons" ></div>');
    infoDiv.appendTo(childDiv);
    buttonsDiv.appendTo(childDiv);

    addPlusButton(buttonsDiv);
  }

  var divTagsInformation = $('<div id="tagInformation_'+divName+'" class="form-group divInformation" ></div>');
  divTagsInformation.appendTo(childDiv);
  $('<h5>Hierarquia percorrida:</h5>').appendTo(divTagsInformation);
  //<button type="button" class="btn btn-outline-primary">Primary</button>
  

  /********** Zona de associar algum atributo *******/

  // Saber se escolhe a propria tag ou algum atributo da tag
  var dropChilds = $('<div class="bodyFields dropdownTagAttr row"></div>');
  dropChilds.appendTo(infoDiv);

  var colTagAttr = $('<div class="col-xs-4"></div>');
  colTagAttr.appendTo(dropChilds);

  var toggleTagAttr = $('<label>' +
    'Associar um atributo: ' +
    '<span class="glyphicon glyphicon-info-sign infoIcon firstInfoIcon" aria-hidden="true" data-toggle="tooltip" '+
    'title="Selecionar se pretende a própria tag ou a tag que contenha um especifico valor de um dos seus atributos"></span>' +
    ' </label>');

  toggleTagAttr.appendTo(colTagAttr);

  var colTagAttr1 = $('<div class="col-xs-7"></div>');
  colTagAttr1.appendTo(dropChilds);

  $('<input type="checkbox" data-toggle="toggle" id="toggle1_' + divName + '" data-size="small">').appendTo(colTagAttr1);
  
  var dropdownAttrNames = $('<span id="dropdownAttrNames_'+ divName +'" class="custom-dropdown dropdown_attrNames"></span>');
  var selectAttrNames = $('<select class="selectAttrNames_'+divName+'"></select>');
  dropdownAttrNames.appendTo(colTagAttr1);
  selectAttrNames.appendTo(dropdownAttrNames);

  //dropdownButton.hide();
  dropdownAttrNames.hide();
 

  if(childs[parent_i].attrs.length > 0){
    // Ir buscar os atributos da Tag
    for(var j = 0 ; j < childs[parent_i].attrs.length; j++){
      selectAttrNames.append('<option>' + childs[parent_i].attrs[j].name + '</option>');
    }

    var dropdownAttrValues = createDropdownAttrValues(childs[parent_i].tag, selectAttrNames[0].value, colTagAttr1);
    dropdownAttrValues.hide();

    $('#toggle1_' + divName).bootstrapToggle({off: 'Desactivado', on: 'Activado'});

    $('#toggle1_' + divName).change(function(){
      if($(this).prop('checked')){
        //dropdownButton.show();
        dropdownAttrNames.show();
        dropdownAttrValues.show();
        
        $(dropdownAttrNames[0].firstChild).change(function(){
          //console.log($(this)[0].value);
          var mainTag = dropdownAttrValues[0].id.split('_')[1];
          //console.log(mainTag);
          //console.log(dropdownAttrValues[0]);
          $(dropdownAttrValues[0]).remove();
          dropdownAttrValues = createDropdownAttrValues(mainTag, $(this)[0].value, $($(this)[0]).closest('.col-xs-7'));
        });


      }else{
        
        dropdownAttrNames.hide();
        dropdownAttrValues.hide();
      }
    });
  } else {
    $('#toggle1_' + divName).bootstrapToggle({off: 'Inexistentes'});
    $('#toggle1_' + divName).bootstrapToggle('disable');
  }

  $('<br>').appendTo(infoDiv);

  /********** Zona de associar a Label *******/
  var childsTag = childs[parent_i].values;

  insertRadioButtonsLabel(infoDiv, divName, childsTag, childs[parent_i].tag);

  /********** Zona de associar a tag com a informacao a apresentar *******/

  // Dropdown com os filhos, caso existam, para saber onde buscar a informacao || input para colocar nome da label
  var dropTextValues = $('<div class="bodyFields textValuesDiv row"></div>');  
  dropTextValues.appendTo(infoDiv);

  var calInfo = $('<div class="col-xs-4"></div>');
  calInfo.appendTo(dropTextValues);

  var toggleInfoValues = $('<label>' +
    'Tag com a informação: ' +
    '<span class="glyphicon glyphicon-info-sign infoIcon" aria-hidden="true" data-toggle="tooltip" '+
    'title="Selecionar qual a tag que contém a informação"></span>' +
    ' </label>');//<input type="checkbox" data-toggle="toggle" id="toggle3_' + divName + '" data-size="small">');
  toggleInfoValues.appendTo(calInfo);

  var calInfo1 = $('<div class="col-xs-7"></div>');
  calInfo1.appendTo(dropTextValues);

  createDivInfoTag(calInfo1, divName, childs[parent_i], "");
  

  /********** Add Plus Button info tag *************/
  //addPlusButtonInfo(calInfo1);

  /********* Scroll + tooltip ********/
  
  $('#childDiv_'+divName+' .textValuesDiv .btn-group, #childDiv_'+divName+' .textValuesDiv .buttonConfig').enscroll({
      showOnHover: false,
      verticalTrackClass: 'track3',
      verticalHandleClass: 'handle3'
  });
  //btngroupButton2.hide();
  $('[data-toggle="tooltip"]').tooltip();



  /************************** Eventos click nos botoes dropdown ********************************/

  // Quando e selecionado um atributo
  $(".dropdownTag li a").click(function(e){
    //e.stopPropagation();

    //console.log("selected: "+ $(this).text().trim());

    $(this).parents(".dropdownTagAttr").find('.attrConfig').html($(this).text().trim() + ' <span class="caret"></span>');
    $(this).parents(".dropdownTagAttr").find('.attrConfig').val($(this).text().trim());

    var idButton = $(this).offsetParent().attr('aria-labelledby');

    $('#'+idButton).parent().siblings('.attrValuesDiv').remove();

    var tag = $(this).parents('div[id^="childDiv_"]').find('b').first().text();

    $(this).parents(".dropdownTagAttr").find('.attrConfig').removeClass('error-validation');
    $(this).parents(".dropdownTagAttr").find('.attrConfig').siblings('p').remove();

    createDropdownAttrValues(tag, $(this), idButton);


  });

  $(".dropTextValues li a, .dropLabelValues li a").click(function(e){
    $(this).parents(".bodyFields").find('.buttonConfig').html($(this).text().trim() + ' <span class="caret"></span>');
    $(this).parents(".bodyFields").find('.buttonConfig').val($(this).text().trim());
    //$(this).parents(".dropdown").find('input[id^="input_drop"]').val($(this).text().trim());

    $(this).parents(".bodyFields").find('.buttonConfig').removeClass('error-validation');
    $(this).parents(".bodyFields").find('.buttonConfig').siblings('p').remove();
  });

  $(".radioTagClicked li a").click(function(e){
    //console.log($(this).offsetParent());
    $(this).offsetParent().siblings(".buttonConfig").html($(this).text().trim() + ' <span class="caret"></span>');
    $(this).offsetParent().siblings(".buttonConfig").val($(this).text().trim());

    $(this).offsetParent().siblings(".buttonConfig").removeClass('error-validation');
    $(this).offsetParent().siblings(".buttonConfig").siblings('p').remove();
  });  

  $('.dropLabelValues input, .dropLabelValues label').click(function(e) {
      e.stopPropagation();
  });
}


/**********************************************************************************/
/*************** Metodos para a configuracao da pag. 2 ****************************/
/**********************************************************************************/

var createDivInfoTag = function(mainDiv, idDiv, tags, counter){
  // mainDiv = <div class = col-xs-7...>
  var colInfo1 = $('<div id="'+idDiv + counter+'" class="infoTagDivs"></div>');
  colInfo1.appendTo(mainDiv);
  addPlusButtonInfo(colInfo1, idDiv+counter, tags);

  var dropdownTagInfo = $('<span id="dropdownTagInfo_'+ idDiv + counter +'" class="custom-dropdown dropdown_tagInfo buttonConfig"></span>');
  var selectTagInfo = $('<select></select>');
  dropdownTagInfo.appendTo(colInfo1);
  selectTagInfo.appendTo(dropdownTagInfo);
  if(counter == ""){
   selectTagInfo.append('<option>' + tags.tag + '</option>');
   colInfo1.find('button').hide();
  }

  var inputToggle2 = $('<input type="checkbox" data-toggle="toggle" id="toggle2_' + idDiv  + counter +'" data-size="small">');
  inputToggle2.appendTo(colInfo1)
  inputToggle2.bootstrapToggle({off: 'Atributo?', on: '+ Atributo'});  

  var dropdownAttrNames2 = $('<span id="dropdownAttrNames2_' + idDiv  + counter +'" class="custom-dropdown dropdown_attrNames2"></span>');
  var selectAttrNames2 = $('<select class="selectAttrNames2_'+ idDiv  + counter +'"></select>');
  selectAttrNames2.appendTo(dropdownAttrNames2);
  dropdownAttrNames2.appendTo(colInfo1); 

  //Parte de mostrar a informacao 
  var divInformation = $($(mainDiv).closest('.tagConfig').find('.divInformation')[0]);
  //console.log(colInfo1.prev());

  // Verificar se tem atributo assinalado
  var parentTag = colInfo1.prev();
  var prevAttr="";
  if(parentTag.length > 0){
    var prevToggle = parentTag.find('.toggle input'); 
    if($(prevToggle).prop('checked')){
      var prevAttrName = parentTag.find('.dropdown_attrNames2 select')[0].value;
      //console.log(prevAttrName);
      var prevAttrValue = parentTag.find('.dropdown_attrValues select')[0].value;
      //console.log(prevAttrValue);
      prevAttr=" "+prevAttrName+"='"+prevAttrValue+"'";
    }
  }


  /******** Colocar Informacao da Hierarquia Percorrida ******/
  if(divInformation.has('div').length == 0){
    divInformation.append('<div class="putInside"><p>&lt;'+ tags.tag + prevAttr +'&gt;<span class="removable"> Informação <span class="endTag">&lt;/'+ tags.tag + '&gt;</span></span></p></div>');
  }else{
    var pToPutInside = divInformation.find('div.putInside');
    var elemToInsert = $('<div class="putInside"><p>&lt;'+ tags.tag + prevAttr + '&gt;<span class="removable"> Informação <span class="endTag">&lt;/'+ tags.tag + '&gt;</span></span></p></div>');
    elemToInsert.appendTo(pToPutInside);
    var endTagInfo = pToPutInside.find('.endTag')[0].outerHTML;
    $(endTagInfo).appendTo(pToPutInside);
    $(pToPutInside[0]).removeClass('putInside');
    $(divInformation.find('span.removable')[0]).remove();
  }

  /******** Sempre que é alterado o toggle button do atributo *********/
  $(inputToggle2).change(function(){
    //console.log($(this));
    var dropAttrName = $(this).parent().siblings('span[id^="dropdownAttrNames2_"]');
    var dropAttrValue = $(this).parent().siblings('span[id^="dropdownAttrValues_"]');
    if($(this).prop('checked')){               
      dropAttrName.show();
      dropAttrValue.show();

      $(selectAttrNames2).change(function(){
        //console.log($(this)[0].value);
        $(this).closest('.infoTagDivs').find('.dropdown_attrValues').remove();
        var tagSelected = $(this).closest('.infoTagDivs').find('.dropdown_tagInfo select')[0].value;
        dropAttrValue = createDropdownAttrValues(tagSelected, $(this)[0].value, colInfo1);
      });
    } else{
      dropAttrName.hide();
      dropAttrValue.hide();
    }
  });


  if(tags.values != null){
    $.each(tags.values, function(index, element){
      selectTagInfo.append('<option id="'+ idDiv + '-'+index+'">' + element.tag + '</option>');
    });

    if(tags.values[0].attrs.length > 0){
      // Ir buscar os atributos da Tag
      for(var j = 0 ; j < tags.values[0].attrs.length; j++){
        selectAttrNames2.append('<option>' + tags.values[0].attrs[j].name + '</option>');
      }      
      var attrvaluesFirst = createDropdownAttrValues(tags.values[0].tag, tags.values[0].attrs[0].name, colInfo1);
      attrvaluesFirst.hide();
    } 
  }  


  $(dropdownTagInfo[0].firstChild).change(function(){
    var selected = $(this)[0];
    //console.log('Alterou na drop a tag');
    //console.log(selected);
    //Verificar qual a tag selecionada
    $.each(selected, function(i, elem){
      if(selected.value == $(elem).text()){
        if(i == 0 && elem.id==""){
          //console.log(elem.id);
          //console.log('A propria tag');
          // Retirar tudo relacionado com atributos
          $(this).closest('span[id^="dropdownTagInfo_"]').siblings('.toggle').hide();
         // dropdownAttrNames2.hide();
          $(this).closest('span[id^="dropdownTagInfo_"]').siblings('span').hide();
          $(this).closest('span[id^="dropdownTagInfo_"]').siblings('button').hide();
          //dropdownAttrValues2.hide();
        } else {
          //console.log(elem);
          var idChild = elem.id.split('-')[1];
          $(this).closest('span[id^="dropdownTagInfo_"]').siblings('button').show();
          //console.log($(elem).closest('div[id^="childDiv_"')[0].id);
          var idParent = $(elem).closest('div[id^="childDiv_"')[0].id.split('_')[1];
          //console.log(idParent + ' - '+idChild);

          // Verificar se a tag selecionada tem atributos 
          var infoChild = tags.values[idChild];

          // So se a tag selecionada tiver filhos é q aparece o botao de add
          if(infoChild.values == null){
            colInfo1.find('button').hide();
          }else{
            colInfo1.find('button').show();
          }

          if(infoChild.attrs.length == 0 ) {
            $(this).closest('span[id^="dropdownTagInfo_"]').siblings('.toggle').hide();
            $(this).closest('span[id^="dropdownTagInfo_"]').siblings('.toggle').find('input').bootstrapToggle('off');
            //$(this).closest('span[id^="dropdownTagInfo_"]').siblings('span').hide();
          }else {
            var spanDropdownTagInfo = $(this).closest('span[id^="dropdownTagInfo_"]');
            spanDropdownTagInfo.siblings('.toggle').show();

            // Alterar os nomes dos atributos para os da tag selecionada
            var tag_dropdownAttrNames = spanDropdownTagInfo.siblings('span[id^="dropdownAttrNames2_"]');
            //tag_dropdownAttrNames.show();
            var selectAttrName = tag_dropdownAttrNames.find('select');
            selectAttrName.empty();
            var attrsChild = tags.values[idChild].attrs;
            //console.log(tags.values[idChild]);
            //console.log(attrsChild);
            $.each(attrsChild, function(i_ch, elem_ch){
              selectAttrName.append('<option>' + elem_ch.name + '</option>');
            });
            //console.log(selectAttrName);
            spanDropdownTagInfo.siblings('span[id^="dropdownAttrValues_"]').remove();
            var dropAttrValue = createDropdownAttrValues(tags.values[idChild].tag, selectAttrName[0].value, colInfo1);
            //dropAttrValue.hide();

            // Quando é alterado o atribudo selecionado
            $(selectAttrName).change(function(){
              //console.log($(this)[0].value);
              spanDropdownTagInfo.siblings('span[id^="dropdownAttrValues_"]').remove();
              dropAttrValue = createDropdownAttrValues(tags.values[idChild].tag, $(this)[0].value, colInfo1);
            });

            var inputToggle = spanDropdownTagInfo.siblings('.toggle').find('input');
            if($(inputToggle).prop('checked')){        
              tag_dropdownAttrNames.show();
              dropAttrValue.show();
            } else{
              tag_dropdownAttrNames.hide();
              dropAttrValue.hide();
            }

            $(inputToggle).change(function(){
              //console.log($(this));
              var dropAttrName = $(this).parent().siblings('span[id^="dropdownAttrNames2_"]');
              var dropAttrValue = $(this).parent().siblings('span[id^="dropdownAttrValues_"]');
              if($(this).prop('checked')){               
                dropAttrName.show();
                dropAttrValue.show();
              } else{
                dropAttrName.hide();
                dropAttrValue.hide();
              }
            });
          }
        }
      }
    });
  });

  if(counter=="" || (tags.values != null && tags.values[0].attrs.length == 0)){
    //console.log('Nao tem attrs');
    inputToggle2.parent().hide();
  }
  dropdownAttrNames2.hide();
}

/***************** Criar a div collapse para cada tag selecionada ******************/
var addCollapseTag = function(childDiv, divName, parent_i){
  var collapseGroup = $('<div class="panel-group" id="accordion' + divName  + '"></div>');
  collapseGroup.appendTo(childDiv);

  var collapseTag = $('<div class="panel panel-default"></div>');
  collapseTag.appendTo(collapseGroup);

  var collapseTag_heading = $('<div class="panel-heading"></div>');
  var collapseTag_heading_h4 = $('<h4 class="panel-title"></h4></div>'); 
  collapseTag_heading.appendTo(collapseTag);
  collapseTag_heading_h4.appendTo(collapseTag_heading);

  var collapseTag_dataToggle = $('<a data-toggle="collapse" data-parent="#accordion' + divName  + '" href="#collapse_' + divName + '"></a> ');
  var collapseTag_dataToggle_span = $('<span id="open_' + divName + '" class="glyphicon glyphicon-menu-down" aria-hidden="true"></span> ');
 
  collapseTag_dataToggle.appendTo(collapseTag_heading_h4);
  collapseTag_dataToggle_span.appendTo(collapseTag_dataToggle);
  collapseTag_dataToggle.append(' <b>' + childs[parent_i].tag + '</b>');

  //var radioButtonid = $('<label style="float:right;"><input type="radio" name="id_infoButton" value="idButton_'+ divName +'"> Identificador de Configuração</label>');
  //radioButtonid.appendTo(collapseTag_heading_h4); 
   
 collapseTag_dataToggle.on('click',function(){
    var arrow = $(this).find('span')[0];
    var myClasses = arrow.classList;

    if(myClasses[1] == 'glyphicon-menu-down'){
      $(arrow).addClass('glyphicon-menu-right');
      $(arrow).removeClass('glyphicon-menu-down');
    }else{
      $(arrow).addClass('glyphicon-menu-down');
      $(arrow).removeClass('glyphicon-menu-right');
    } 
  });
  
  
  var collapseTag_div = $('<div id="collapse_' + divName + '" class="panel-collapse collapse in" aria-expanded="true"></div>');
  collapseTag_div.appendTo(collapseTag);
  var collapseTag_body = $('<div id="body' + divName + '" class="panel-body tagConfig"></div>');
  collapseTag_body.appendTo(collapseTag_div);

  return collapseTag_body;
}

var addPlusButtonInfo = function(divToPut, idDiv, tags){
  var addSameTag2 = $('<button type="button" id="' + idDiv +'_addInfoTag" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> </button>');
  addSameTag2.appendTo(divToPut);

  if(tags.values == null || tags.values[0].values == null){
    addSameTag2.hide();
  }

  addSameTag2.click(function(){

    // NAo permitir alterar valores anteriores
    $(this).parent('.infoTagDivs').find('select').attr('disabled','disabled');
    $(this).parent('.infoTagDivs').find('.toggle input').bootstrapToggle('disable');
    $(this).attr('disabled','disabled');

    /** Buscar a informação ja selecionada **/

    // Buscar o index da tag
    var realIdTag = $(this).closest('.infoTagDivs')[0].id;
    var idMainTagInfo = realIdTag.split(/\_|\+/g)[1];

    // Buscar a tag filha seleccionada (Nome)
    var nameDescTag = $(this).siblings('.dropdown_tagInfo').find('select')[0].value;
    
    var tagToInfo = childs[idMainTagInfo].values;
    // Ver em que posicao esta está no vector para ir buscar os seus filhos

    var idMainTag = realIdTag.split('+');

    //findIndexTagName(childs, idMainTagInfo, nameDescTag);
    //console.log(descendents);

    for(var i=1; i < idMainTag.length; i++){
      var tagInfo_aux = tagToInfo;
      tagToInfo = tagInfo_aux[idMainTag[i]].values;
    }

    var descendents = findIndexTagName(tagToInfo, nameDescTag);
    

    var mainDiv = $(this).closest('.col-xs-7')[0];
    
    createDivInfoTag(mainDiv, idDiv, descendents[1], "+"+descendents[0]);
  });

}

var findIndexTagName = function(xmlElems, tagName){
  var result = null;
  $.each(xmlElems, function(i, elem){
    if(elem.tag == tagName){
      result = [i, elem];
      return false; // So assim e que para a execucao do loop
    }
  }); 
  return result;
}

var addPlusButton = function(divToPut){
  var addSameTag2 = $('<button type="button" id="' + divToPut.attr("id") +'_addTag" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> </button>');
  addSameTag2.appendTo(divToPut);

  addSameTag2.click(function(){
    var parent = this.parentElement.parentElement;

    // Buscar o i do Id da tag
    var parent_i = parent.id.split("_")[1];

    //var clnDiv = $('<div class="'+parent.id+'_child"></div>');
    var child_i = tagsCounter[parent_i];
    tagsCounter[parent_i]= child_i + 1;

    createTagConfig(parent.id+"_"+child_i, parent, true);
  });
}

var addMinusButton = function(divToPut){
  var removeTag = $('<button type="button" id="' + divToPut.attr("id") +'_removeTag" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> </button>');
  removeTag.appendTo(divToPut);

  removeTag.click(function(){
    //console.log($(this.parentElement.parentElement));
    $(this.parentElement.parentElement).prev().remove(); // Remove o <hr>
    $(this.parentElement.parentElement).remove();
  });
}

var change_checkbox = function() {
  $('.panel-heading .list-group-item').each(function () { 
    // Settings
    var $widget = $(this),
        $checkbox = $('<input type="checkbox" class="hidden" />'),
        color = ($widget.data('color') ? $widget.data('color') : "primary"),
        style = ($widget.data('style') == "button" ? "btn-" : "list-group-item-"),
        settings = {
            on: {
                icon: 'glyphicon glyphicon-check'
            },
            off: {
                icon: 'glyphicon glyphicon-unchecked'
            }
        };
        
    $widget.css('cursor', 'pointer')
    $widget.append($checkbox);

    // Event Handlers
    $widget.on('click', function () {
        $checkbox.prop('checked', !$checkbox.is(':checked'));
        $checkbox.triggerHandler('change');
        updateDisplay();
    });
    $checkbox.on('change', function () {
        updateDisplay();
    });
      

    // Actions
    function updateDisplay() {
        var isChecked = $checkbox.is(':checked');

        // Set the button's state
        $widget.data('state', (isChecked) ? "on" : "off");

        // Set the button's icon
        $widget.find('.state-icon')
            .removeClass()
            .addClass('state-icon ' + settings[$widget.data('state')].icon);

        // Update the button's color
        if (isChecked) {
            $widget.addClass(style + color + ' active');
        } else {
            $widget.removeClass(style + color + ' active');
        }
    }

    // Initialization
    function init() {
        
        if ($widget.data('checked') == true) {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
        }
        
        updateDisplay();

        // Inject the icon if applicable
        if ($widget.find('.state-icon').length == 0) {
            $widget.prepend('<span class="state-icon ' + settings[$widget.data('state')].icon + '"></span>');
        }
    }
    init();
  });

  $('#get-checked-data').on('click', function(event) {
    event.preventDefault(); 
    var checkedItems = {}, counter = 0;
    $("#check-list-box li.active").each(function(idx, li) {
        checkedItems[counter] = $(li).text();
        counter++;
    });
    $('#display-json').html(JSON.stringify(checkedItems, null, '\t'));
  });
}

var insertRadioButtonsLabel = function(childDiv, divName, childTags, tagName){

  // Dropdown com os filhos, caso existam, para saber onde buscar a label || input para colocar nome da label
  var dropLabelValues = $('<div class="bodyFields labelValuesDiv row"></div>');     
  dropLabelValues.appendTo(childDiv);

  var calLabel = $('<div class="col-xs-4"></div>');
  calLabel.appendTo(dropLabelValues);

  var toggleLabel = $('<label>' +
    'Label a associar: ' +
    '<span class="glyphicon glyphicon-info-sign infoIcon" aria-hidden="true" data-toggle="tooltip" '+
    'title="Selecionar se pretende que apareça o nome de alguma tag ou algum dos valores existentes"></span></label><br>');
    // + ' <input type="checkbox" data-toggle="toggle" id="toggle2_' + divName + '"></label>');
  toggleLabel.appendTo(calLabel);

  var radio_div = $('<div id="radio-'+divName+'" class="col-xs-7 radioDiv"></div>');
  radio_div.appendTo(dropLabelValues);

  if(childTags != null){
    var divTagLabel = $('<div id="checkDiv0-'+divName+'"</div>');
    divTagLabel.appendTo(radio_div);
    var tagLabel = $('<label data-toggle="collapse" data-target="#c0-'+divName+'"><span id="check0-'+divName+'"class="glyphicon glyphicon-unchecked" aria-hidden="true"></span> Uma tag filha</label>');
    tagLabel.appendTo(divTagLabel);
    var tagLabelCollapse = insertTagLabelConfig(divName, divTagLabel, childTags);

    var divTagInfoLabel = $('<div id="checkDiv1-'+divName+'"</div>');
    divTagInfoLabel.appendTo(radio_div);
    var tagInfoLabel = $( '<label data-toggle="collapse" data-target="#c1-'+divName+'"><span id="check1-'+divName+'"class="glyphicon glyphicon-unchecked" aria-hidden="true"></span> Valor de uma tag filha</label>');
    tagInfoLabel.appendTo(divTagInfoLabel);
    var tagInfoLabelCollapse = insertTagInfoLabelConfig(divName, divTagInfoLabel, childTags);

    var divTagOtherLabel = $('<div id="checkDiv2-'+divName+'"</div>');
    divTagOtherLabel.appendTo(radio_div);
    var tagOtherLabel = $( '<label data-toggle="collapse" data-target="#c2-'+divName+'"><span id="check2-'+divName+'"class="glyphicon glyphicon-unchecked" aria-hidden="true"></span> Outra</label>');
    tagOtherLabel.appendTo(divTagOtherLabel);  

    var otherLabelCollapse = $('<div id="c2-'+divName+'" class="collapse">' +
      '<input class="buttonConfig" placeholder="Nome da Label" type="text" id="newLabel_'+ divName +'" value="" class="form-control"/>' + 
      '</div>');
    otherLabelCollapse.appendTo(divTagOtherLabel);

    /************** Eventos *************/
    // Quando se clica numa checkbox

    tagLabel.on('click', function(){

      var id_checkbox = $(this).find('span').attr('id');
      var checkbox_elem = $('#'+id_checkbox);

      var id_child = id_checkbox.split("-");

      var check1_id = 'check1-'+id_child[1];
      var check2_id = 'check2-'+id_child[1];
      //alterar a checkbox
      var myClasses = checkbox_elem[0].classList;

      if(myClasses[1] == 'glyphicon-unchecked'){
        $(checkbox_elem).removeClass('glyphicon-unchecked');
        $(checkbox_elem).addClass('glyphicon-check');
        $(checkbox_elem).css("color", "#337AB7");

        $('#'+check1_id).removeClass('glyphicon-check');
        $('#'+check1_id).addClass('glyphicon-unchecked');
        $('#'+check1_id).css("color", "white");

        $('#'+check2_id).removeClass('glyphicon-check');
        $('#'+check2_id).addClass('glyphicon-unchecked');
        $('#'+check2_id).css("color", "white");

        $('#c1-'+id_child[1]).collapse('hide');
        $('#c2-'+id_child[1]).collapse('hide');

        $(this).parents('.labelValuesDiv').removeClass('error-validation');
        $(this).parents('.labelValuesDiv').siblings('p').remove();
      }
    });

    tagInfoLabel.on('click', function(){

      var id_checkbox = $(this).find('span').attr('id');
      var checkbox_elem = $('#'+id_checkbox);

      var id_child = id_checkbox.split("-");

      var check0_id = 'check0-'+id_child[1];
      var check2_id = 'check2-'+id_child[1];
      //alterar a checkbox
      var myClasses = checkbox_elem[0].classList;

      if(myClasses[1] == 'glyphicon-unchecked'){
        $(checkbox_elem).removeClass('glyphicon-unchecked');
        $(checkbox_elem).addClass('glyphicon-check');
        $(checkbox_elem).css("color", "#337AB7");

        $('#'+check0_id).removeClass('glyphicon-check');
        $('#'+check0_id).addClass('glyphicon-unchecked');
        $('#'+check0_id).css("color", "white");

        $('#'+check2_id).removeClass('glyphicon-check');
        $('#'+check2_id).addClass('glyphicon-unchecked');
        $('#'+check2_id).css("color", "white");
          
        $('#c0-'+id_child[1]).collapse('hide');
        $('#c2-'+id_child[1]).collapse('hide');

        $(this).parents('.labelValuesDiv').removeClass('error-validation');
        $(this).parents('.labelValuesDiv').siblings('p').remove();
      }
    });

    tagOtherLabel.on('click', function(){

      var id_checkbox = $(this).find('span').attr('id');
      var checkbox_elem = $('#'+id_checkbox);

      var id_child = id_checkbox.split("-");

      var check0_id = 'check0-'+id_child[1];
      var check1_id = 'check1-'+id_child[1];
      //alterar a checkbox
      var myClasses = checkbox_elem[0].classList;

      if(myClasses[1] == 'glyphicon-unchecked'){
        $(checkbox_elem).removeClass('glyphicon-unchecked');
        $(checkbox_elem).addClass('glyphicon-check');
        $(checkbox_elem).css("color", "#337AB7");

        $('#'+check0_id).removeClass('glyphicon-check');
        $('#'+check0_id).addClass('glyphicon-unchecked');
        $('#'+check0_id).css("color", "white");

        $('#'+check1_id).removeClass('glyphicon-check');
        $('#'+check1_id).addClass('glyphicon-unchecked');
        $('#'+check1_id).css("color", "white");

        $('#c0-'+id_child[1]).collapse('hide');
        $('#c1-'+id_child[1]).collapse('hide');

        $(this).parents('.labelValuesDiv').removeClass('error-validation');
        $(this).parents('.labelValuesDiv').siblings('p').remove();

        $('#c2-'+id_child[1]).find('input').keyup(function(e) {
          if($(this).val()){
            $(this).addClass('ok-validation');
            $(this).removeClass('error-validation');
            $(this).siblings('p').remove();
          } else {
            $(this).removeClass('ok-validation');
            $(this).addClass('error-validation');
            $(this).siblings('p').remove();
            $('<p class="errorp" style="color: red;"> <strong> Campo obrigatório! </strong> </p>').insertAfter($(this));

          }
        });
      }
    });
  }else{
    var divTagLabel = $('<div id="checkDiv1-'+divName+'"></div>');
    divTagLabel.appendTo(radio_div);
    var tagLabel = $('<label><span id="check1-'+divName+'" style="color: #337AB7" class="glyphicon glyphicon-check"></span>' + tagName+ '</label>');
    tagLabel.appendTo(divTagLabel);

    var otherLabelDiv = $('<div id="checkDiv2-'+divName+'"></div>');
    otherLabelDiv.appendTo(radio_div); 
    var otherLabel = $('<label><span id="check2-'+divName+'" class="glyphicon glyphicon-unchecked" aria-hidden="true"></span> </label> ');
    otherLabel.appendTo(otherLabelDiv);

    var otherLabelInput = $(' <input class="buttonConfig" placeholder="Nome da Label" type="text" id="newLabel_'+ divName +'" value="" class="form-control"/>' + 
      '</div>');
    otherLabelInput.appendTo(otherLabelDiv);

    // Eventos 

    tagLabel.on('click', function(){

      var id_checkbox = $(this).find('span').attr('id');
      var checkbox_elem = $('#'+id_checkbox);

      var id_child = id_checkbox.split("-");
      var check2_id = 'check2-'+id_child[1];
      //alterar a checkbox
      var myClasses = checkbox_elem[0].classList;

      if(myClasses[1] == 'glyphicon-unchecked'){
        $(checkbox_elem).removeClass('glyphicon-unchecked');
        $(checkbox_elem).addClass('glyphicon-check');
        $(checkbox_elem).css("color", "#337AB7");

        $('#'+check2_id).removeClass('glyphicon-check');
        $('#'+check2_id).addClass('glyphicon-unchecked');
        $('#'+check2_id).css("color", "white");

        $('#checkDiv2-'+id_child[1]).find('p').remove();
        $('#checkDiv2-'+id_child[1]).find('input').removeClass('error-validation');
        $('#checkDiv2-'+id_child[1]).find('input').removeClass('ok-validation');
      }
    });

    otherLabel.on('click', function(){
      var id_checkbox = $(this).find('span').attr('id');
      var checkbox_elem = $('#'+id_checkbox);

      var id_child = id_checkbox.split("-");
      var check1_id = 'check1-'+id_child[1];
      //alterar a checkbox
      var myClasses = checkbox_elem[0].classList;

      if(myClasses[1] == 'glyphicon-unchecked'){
        $(checkbox_elem).removeClass('glyphicon-unchecked');
        $(checkbox_elem).addClass('glyphicon-check');
        $(checkbox_elem).css("color", "#337AB7");

        $('#'+check1_id).removeClass('glyphicon-check');
        $('#'+check1_id).addClass('glyphicon-unchecked');
        $('#'+check1_id).css("color", "white");


        $('#checkDiv1-'+id_child[1]).find('label').removeClass('ok-validation');
      }
    });

    otherLabelInput.keyup(function(e) {
       var checkbox_otherLabel = $(this).parent().find('span');
        checkbox_otherLabel.removeClass('glyphicon-unchecked');
        checkbox_otherLabel.addClass('glyphicon-check');
        checkbox_otherLabel.css("color", "#337AB7");
        
        var split_id = checkbox_otherLabel.attr('id').split('-')[1];
        $('#check1-'+split_id).removeClass('glyphicon-check');
        $('#check1-'+split_id).addClass('glyphicon-unchecked');
        $('#check1-'+split_id).css("color", "white");

        $('#checkDiv1-'+split_id).find('label').removeClass('error-validation');
        $('#checkDiv1-'+split_id).find('label').removeClass('ok-validation');

      if($(this).val()){
        $(this).addClass('ok-validation');
        $(this).removeClass('error-validation');
        $(this).siblings('p').remove();
      } else {
        $(this).removeClass('ok-validation');
        $(this).addClass('error-validation');
        $(this).siblings('p').remove();
        $('<p class="errorp" style="color: red;"> <strong> Campo obrigatório! </strong> </p>').insertAfter($(this));
      }
    });
  }

  $('<br>').appendTo(childDiv);
}

var insertTagLabelConfig = function(divName, divToInsert, childTags){
  var divContainer0 = $('<div id="c0-'+divName+'" class="collapse"></div>');
  divContainer0.appendTo(divToInsert);

  var dropdownTagLabel = $('<span id="dropdownTagLabel_'+ divName +'" class="custom-dropdown dropdown_tagLabel buttonConfig"></span>');
  var selectTagLabel = $('<select class="selectTagLabel_'+ divName +'"></select>');
  dropdownTagLabel.appendTo(divContainer0);
  selectTagLabel.appendTo(dropdownTagLabel);

  // Ir buscar os atributos da Tag
  $.each(childTags, function(index, element){
    selectTagLabel.append('<option>' + element.tag + '</option>');
  });

  $('.selectTagLabel_'+divName).change(function(){
    //console.log($(this)[0].value);
  });

}

var insertTagInfoLabelConfig = function(divName, divToInsert, childTags){
  var divContainer1 = $('<div id="c1-'+divName+'" class="collapse"></div>');
  divContainer1.appendTo(divToInsert); 

  var dropdownTagInfoLabel = $('<span id="dropdownTagLabel'+ divName +'" class="custom-dropdown dropdown_tagInfoLabel buttonConfig"></span>');
  var selectTagInfoLabel = $('<select></select>');
  dropdownTagInfoLabel.appendTo(divContainer1);
  selectTagInfoLabel.appendTo(dropdownTagInfoLabel);

  // Ir buscar os atributos da Tag
  $.each(childTags, function(index, element){
    selectTagInfoLabel.append('<option>' + element.tag + '</option>');
  });
}